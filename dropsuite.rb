require 'find'
require 'yaml'
class Integer
    def to_filesize
      {
        'B'  => 1024,
        'KB' => 1024 * 1024,
        'MB' => 1024 * 1024 * 1024,
        'GB' => 1024 * 1024 * 1024 * 1024,
        'TB' => 1024 * 1024 * 1024 * 1024 * 1024
      }.each_pair { |e, s| return "#{(self.to_f / (s / 1024)).round(2)}#{e}" if self < s }
    end
  end

yml_file = YAML.load_file('config.yml')

files = Array.new
contents = Array.new
basename = Array.new
size = Array.new

print "Enter valid path or empty for default config: "
a = gets.chomp
@c
loop do
  print "Show with path ( yes or no): "
  b = gets.chomp
  b = b.downcase
  @c = b
  if b.empty?
    puts "No input."
  elsif b.include?("yes")
    break
  elsif b.include?("no")
    break
  else
    puts "Please input 'yes' or 'no'"
  end
end

pathi = a.empty? ? yml_file['path'] : a
Find.find(pathi) do |e|
    g = e if File.directory?(e)
    d = g unless g == pathi
    next if e == g or e == pathi+"/dropsuite.rb"
    next if e == pathi+"/config.yml"
    files << e unless File.directory?(e)
end

files.each do |file|
    f = File.open(file, 'r')
    contents << f.read
    basename << "      "+File.basename(file) +" ===> " + File.size(file).to_filesize
    size << "       "+(file)+" ==> "+(File.size(file).to_filesize)
    f.close
end

numbers = Array.new
content = contents.select {|c| contents.count(c)}.uniq
content.each do |co|
  s = Hash.new
  s[:content] = co
  s[:numb] = contents.select {|c| c == co ? contents.count(c): nil}.size
  numbers << s
end
last_num = (numbers.sort_by{|k| k[:numb]}).last

puts "\n"
puts "     Test Script Dropsuite"
puts "\n"
puts "    - Same content:"
puts "         "+last_num[:content]+" "+last_num[:numb].to_s
puts "\n"
puts "    - Contents Size:\n"
puts @c == 'yes' ? size : basename